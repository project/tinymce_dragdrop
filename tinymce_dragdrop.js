
/**
 * Extend tabledrag.js's default row swapping behavior to take into account
 * that tinymce editors are in an iframe, and iframes get reloaded when moved
 * in the dom.
 */
if (Drupal.tableDrag) {
  $.aop.around({target: Drupal.tableDrag.prototype.row.prototype, method: 'swap'}, function(invocation) {
    // TODO: What's the proper way to handle situation where this.group has
    // multiple items?
    if (this.group.length > 1) {
      return invocation.proceed();
    }
    
    // If we don't have tinymce on this page, we don't need to do anything special.
    if (!window.tinyMCE) {
      return invocation.proceed();
    }
    
    var position = invocation.arguments[0];
    var referenceRow = invocation.arguments[1];
    var thisRow = this.group[0];
    var editorIds = [];
    for (var id in window.tinyMCE.editors) {
      editorIds.push(id);
    }
    var editors = $('#' + editorIds.join(', #'), thisRow);
    var result;
    var settings = [];
    
    // If there aren't any tinymce editors in this row, we don't need to do anything special.
    if (!editors.length) {
      return invocation.proceed();
    }
    
    // If thisRow is the same as referenceRow, then nothing will happen during a swap.
    // However, letting the default swap behavior run will result in the editor's iframe
    // being reloaded and messed up, so instead, we just force nothing to happen.
    if (thisRow == referenceRow) {
      return;
    }
    
    // Remove editors
    editors.each(function() {
      settings[this.id] = window.tinyMCE.get(this.id).settings;
      window.tinyMCE.get(this.id).remove();
    });
    
    // Default tabledrag behavior
    result = invocation.proceed();
    
    // Re-initialize editors
    editors.each(function() {
      window.tinyMCE.init(settings[this.id]);
    });
    
    // Return what the default behavior would have returned.
    return result;
  });
}
